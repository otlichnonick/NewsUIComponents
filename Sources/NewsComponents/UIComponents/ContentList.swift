//
//  ContentList.swift
//  SecondHomeworkApp
//
//  Created by Anton Agafonov on 05.07.2022.
//

import SwiftUI
import NewsModel

public struct ContentList: View {
    var list: [DataModel]
    @Binding var selectedNews: DataModel?
    let onTap: (DataModel) -> Void
    let onBottomList: () -> Void
    
    public init(list: [DataModel],
                selectedNews: Binding<DataModel?>,
                onTap: @escaping (DataModel) -> Void,
                onBottomList: @escaping () -> Void) {
        self.list = list
        self._selectedNews = selectedNews
        self.onTap = onTap
        self.onBottomList = onBottomList
    }
    
    public var body: some View {
        ScrollView {
            LazyVStack(spacing: 12) {
                ForEach(list, id: \.self.uuid) { newsItem in
                    NewsCell(cellData: newsItem)
                        .onTapGesture {
                            onTap(newsItem)
                        }
                        .onAppear {
                            if newsItem.uuid == list.last?.uuid {
                                onBottomList()
                            }
                        }
                }
            }
        }
    }
}
