//
//  NewsCell.swift
//  SecondHomeworkApp
//
//  Created by Anton Agafonov on 08.07.2022.
//

import SwiftUI
import NewsModel

public struct NewsCell: View {
    var cellData: DataModel
    
    public init(cellData: DataModel) {
        self.cellData = cellData
    }
    
    public var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(cellData.title)
                    .font(.body)
                
                Text(cellData.description ?? "")
                    .font(.footnote)
            }
            .multilineTextAlignment(.leading)
            .fixedSize(horizontal: false, vertical: true)
            
            Spacer()
        }
        .padding()
        .background(Color.green)
        .cornerRadius(10)
    }
}
